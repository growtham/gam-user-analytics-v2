# What does this code do? 
- The script stores data about website visitors in the browser and makes it accessible via a simple javascript object. 
- User analytics is typically used to capture and send UTMs and other URL parameters to the backend or directly to analytics platforms such as Mixpanel and Segment.

# Features

## 1. Persistence with Local Storage and Cookie
- If the data can be stored in the browser's local storage, it will be saved and loaded from there. Otherwise, it will be loaded from a cookie. If there is no cookie, an empty object will be returned.
- You can set the data to expire after a chosen amount of time.
- If you define a 'top domain' in the configuration settings, the script will track data across that domain and its subdomains. To do so, a cross domain cookie is used, and the browser's local storage is being updated with data from that cookie.

## 2. Default Values

- **referrer**: returns the URL of the referring domain. 
- **landing_page_first**: the first page the user visits.
- **landing_page_last**: the last page the user was on before form submission, or the last page of the session. A session expires after 30 minutes of inactivity.
- **last_page_seen**: the last page the user visits.
- **first_visit_time**: the first date and time a user was seen.
- **last_visit_time**: the last date and time the user was seen.
- **page_views**: the number of pages the user viewed. 
- **IP address**: the user's IP address.
- **city**: the user's city, derived from his IP.
- **country**: the user's country, derived from his IP.
- **ga_cid**: Google Analytics client ID 

User geolocation information is fetched from an external API. The query runs once per day per user.
This is a free service so it may exceed its quota at times. Reach out to us to activate the paid service for more reliable geolocation data.

## 3. Marketing Attribution 

- Parameters defined in `urlParams` will be taken from the URL and updated in the GAM object.
- the URL parameters present in the first visit time will be recorded as first touch. 
- the URL parameters present in the last visit time will be recorded as last touch. 
- If there is no value for utm_source, but there is a referrer value, utm_source will receive the referrer value.
- In order for last touch values to update, there needs to be at least one new value. Therefore, if the referrer is empty and there are no relevant parameters in the URL, last touch values will not be updated.

Here are the common URL parameters used:

- **utm_source**: UTM variable generally used to track the type of traffic (e.g. organic or paid).
- **utm_medium**: UTM variable generally used to track the traffic medium (e.g. email, cpc or display).
- **utm_campaign**: UTM variable generally used to track the advertising campaign.
- **utm_content**: UTM variable generally used to track ads.
- **utm_term**: UTM variable generally used to track keywords.

## 4. URL Decorator
The last touch GAM object values will be transferred to URLs on the webpage, as defined in gam.config.urlDecorator:

- urlsToDecorate defines which URLs on the page will be decorated. The URLs are selected based on the hostname only.
- By default all values in `urlParams` are are passed to the decorated URLs. 
- To pass additional values from the current page URL to the decorated URLs, use `queryParams`.
- Please note that URLs that point to the same host name as the current browser page will not be decorated by default. For example, if a user is browsing a page on example.com, the links on that page that point to example.com will not be decorated. This behavior can be circumvented with a setting (see Configuration Settings below).


## 5. Form fields populator
You can populate form fields on your site with values from the GAM object by using a simple function that is shipped with the script. See 'Implementation' below for instructions and an example. Please note that jQuery is required for this function to work.

## 6. Adblock circumvention

Adblockers are browser add-ons which block ads and tracking scripts, and are used by millions of users. Currently, GAM isn't blocked by adblockers, but the Google Tag Manager is. If you want to benefit from the extra reach to adblock users, we provide a WordPress plugin to allow you to load GAM without the use of GTM. If your website is not WordPress based, we can provide you with a custom solution.

## 7. Configuration Settings
- **debug**: accepts true or false. True tracks the script progress in the browser console. Change it to false for production sites.
- **cookieExpiry**: defines the number of days after which the cookie will expire. Accepts fractions as well.
- ** topDomain ** : if you define a top domain, the script will track data across this domain and all its subdomains.
- **decorateOnOwnDomain** in the **urlDecorator** settings: This will allow decorating URLs which point to the same host as the current browser page (see URL Decorator section above).

## 8. System Paremeters
- **hasFirst**: shows whether the storage already has first touch values.
- **lastGeo**: timestamp for when was the last geolocation fetch, which is done every 24h.
- **session_duration**: determines the duration of the user session. this is only used to calculate the landing_page_last at the moment. Set to 30 minutes.


# Implementation

- gam.js must be loaded, followed by gam.config.
- define `urlParams` to store the URL parameters for which you want to track first touch and last touch.
- define `urlsToDecorate` to identify which URLs on the page will be decorated. 
- define `queryParams` to pass additional page URL params you want to pass to the decorated URLs.

Once the code is setup, the variables can be accessed through the `gamUserTracking` object. For example, to access the first touch utm_term value, use  `gamUserTracking.utm_term_first`


# Installation of the WordPress plugin

If you chose to use the WordPress plugin instead of GTM, please follow the installation instructions:

- Under WordPress Admin, goto Plugins => Add New.
- Upload and install the plugin.
- Proceed to Settings -> GAM JS Settings.
- Paste in the desired gam.config values and click Save Changes.


** Reach out to yalcin@growthanalyticsmarketing.com for a sample GTM container or for the WordPress plugin. **

 
# Example 
- Here's a complete example with some commonly used variables:
~~~
  <script src="https://scripts.attriva.com/gam.js"></script>
  <script>
    gam.config = {
      debug : false,
      cookieExpiry : 365, // sets the cookie expiry in days
      topDomain : 'example.com',

      urlParams : [
       'utm_source',
       'utm_medium',
       'utm_campaign',
       'utm_content',
       'utm_term'
      ],

      urlDecorator : {
        decorateOnOwnDomain: false,
        
        // use only host names
        urlsToDecorate : [
  		  'growthanalyticsmarketing.com',
		  'example.org'
	    ],

        // variables you want to pass from the current site url to the decorated urls
        queryParams : [
		  'gclid','fbclid','msclkid'
	     ]
      }
    };
  </script>
~~~

# Populating form fields with values from the GAM object

- Please note that jQuery is required for this functionality.

- Create the relevant form fields in your form (these would usually be hidden fields). The form field names must match the value you want to pass from the GAM object plus a '_gam' suffix.

For example, to pass the utm_source_first value into the form, you should create a form field as follows:
~~~
<input type="hidden" name="utm_source_first_gam">
~~~

The next stage is to trigger the population function on form submit. For example:
~~~
<form id="form1">
	...
	<input type="hidden" name="utm_source_first_gam">
	...
</form>
<script>
var form = jQuery('#form1');
form.submit(function(){
	if ( typeof(gam) !== 'undefined' ){
		gam.populateFormFields(jQuery(this));
	}
});
</script>
~~~

If you are using Hubspot for your forms, you trigger the population function inside the form embed code, as follows:
~~~
hbspt.forms.create({
	...
     formId: '...',
     onFormSubmit:function($form){
        if ( typeof(gam) !== 'undefined' ){
			gam.populateFormFields($form);
		}
     }
});
~~~
