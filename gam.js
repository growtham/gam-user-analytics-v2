<script>
  (function(window) {
    // Create debug functions in global scope
    var debugLog = function(message, data) {
      if (window.GAMConfig && window.GAMConfig.debug) {
        console.log('GAM Debug:', message, data || '');
      }
    };
  
    var errorLog = function(message, error) {
      if (window.GAMConfig && window.GAMConfig.debug) {
        console.error('GAM Error:', message, error || '');
      }
    };
  
    try {
      debugLog('Starting GAM initialization');
  
      // Core Configuration
      window.GAMConfig = {
        debug: false, // Set to true for debugging
        cookieExpiry: 365,
        topDomain: 'test.com',
        sessionLength: 30,
        urlParams: [
          'utm_source',
          'utm_medium',
          'utm_campaign',
          'utm_content',
          'utm_term',
          'utm_id',
          'utm_source_platform',
          'utm_creative_format',
          'utm_marketing_tactic'
        ],
        urlDecorator: {
          decorateOnOwnDomain: false,
          urlsToDecorate: ['test.com'],
          queryParams: ['gclid', 'fbclid', 'msclkid']
        }
      };
  
      debugLog('Config initialized', window.GAMConfig);
  
      // Constructor function
      var GAM = function(config) {
        try {
          debugLog('Creating GAM instance');
          this.config = config;
          this.storage = this.initializeStorage();
          debugLog('Storage initialized');
          this.init();
        } catch (e) {
          errorLog('Error in GAM constructor', e);
        }
      };
  
      GAM.prototype.getParameterByName = function(name) {
        try {
          debugLog('Getting URL parameter', name);
          name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
          var regex = new RegExp('[?&]' + name + '=([^&#]*)');
          var results = regex.exec(window.location.search);
          if (!results) return null;
          var value = decodeURIComponent(results[1].replace(/\+/g, ' '));
          debugLog('URL parameter found', { name: name, value: value });
          return value;
        } catch (e) {
          errorLog('Error getting URL parameter', e);
          return null;
        }
      };
  
      GAM.prototype.collectUrlParameters = function() {
        try {
          debugLog('Collecting URL parameters');
          var params = {};
          var hasParams = false;
  
          // First, check if any UTM parameter exists in the current URL
          var hasAnyUtmParam = this.config.urlParams.some(function(param) {
            return this.getParameterByName(param) !== null;
          }.bind(this));
  
          // If we have any UTM parameter, reset all UTM '_last' values
          if (hasAnyUtmParam) {
            debugLog('New UTM parameters detected, resetting all UTM last values');
            this.config.urlParams.forEach(function(param) {
              params[param + '_last'] = null;
            });
          }
  
          // Now collect current UTM parameters
          this.config.urlParams.forEach(function(param) {
            var value = this.getParameterByName(param);
            if (value) {
              params[param + '_last'] = value;
              hasParams = true;
            }
          }.bind(this));
  
          // Handle other tracking parameters (non-UTM)
          this.config.urlDecorator.queryParams.forEach(function(param) {
            var value = this.getParameterByName(param);
            if (value) {
              params[param + '_last'] = value;
              hasParams = true;
            }
          }.bind(this));
  
          debugLog('Collected URL parameters', params);
          return hasParams ? params : null;
        } catch (e) {
          errorLog('Error collecting URL parameters', e);
          return null;
        }
      };
  
      // Prototype methods
      GAM.prototype.initializeStorage = function() {
        debugLog('Initializing storage');
        var self = this;
        return {
          get: function(key) {
            try {
              debugLog('Getting storage for key', key);
              var localData = !self.config.disallowLocalStorage && window.localStorage ? 
                window.localStorage.getItem(key) : null;
              var cookieData = self.readCookie(key);
              debugLog('Storage data retrieved', { localData: localData, cookieData: cookieData });
              return self.resolveStorageData(localData, cookieData);
            } catch (e) {
              errorLog('Error getting storage', e);
              return '{}';
            }
          },
          set: function(key, value, onlyLocal) {
            try {
              debugLog('Setting storage', { key: key, value: value, onlyLocal: onlyLocal });
              var data = JSON.stringify(value);
              
              if (!self.config.disallowLocalStorage && window.localStorage) {
                window.localStorage.setItem(key, data);
                debugLog('LocalStorage set successfully');
              }
  
              if (!onlyLocal) {
                self.setCookie(key, data);
                debugLog('Cookie set successfully');
              }
            } catch (e) {
              errorLog('Error setting storage', e);
            }
          }
        };
      };
  
      GAM.prototype.readCookie = function(name) {
        try {
          debugLog('Reading cookie', name);
          var nameEQ = name + '=';
          var cookies = document.cookie.split(';');
          
          for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            while (cookie.charAt(0) === ' ') {
              cookie = cookie.substring(1);
            }
            if (cookie.indexOf(nameEQ) === 0) {
              var value = decodeURIComponent(cookie.substring(nameEQ.length));
              debugLog('Cookie found', { name: name, value: value });
              return value;
            }
          }
          debugLog('Cookie not found', name);
          return null;
        } catch (e) {
          errorLog('Error reading cookie', e);
          return null;
        }
      };
  
      GAM.prototype.setCookie = function(name, value) {
        try {
          var expiry = new Date();
          expiry.setTime(expiry.getTime() + (86400000 * this.config.cookieExpiry));
          var domain = this.config.topDomain ? 'domain=' + this.config.topDomain + ';' : '';
          document.cookie = name + '=' + encodeURIComponent(value) + '; expires=' + expiry.toGMTString() + '; path=/; ' + domain;
          debugLog('Cookie set', { name: name, value: value });
        } catch (e) {
          errorLog('Error setting cookie', e);
        }
      };
  
      GAM.prototype.resolveStorageData = function(localData, cookieData) {
        return localData || cookieData || '{}';
      };
  
      GAM.prototype.trackPageView = function() {
        try {
          debugLog('Starting page view tracking');
          var tracking = JSON.parse(this.storage.get('gam_user_tracking') || '{}');
          debugLog('Current tracking data', tracking);
  
          var updates = {
            last_page_seen: window.location.pathname,
            visit_time_last: new Date().toISOString(),
            pageviews: (tracking.pageviews || 0) + 1,
            user_agent: navigator.userAgent,
            ga_cid: this.readCookie('_ga') || ''
          };
  
          // Collect URL parameters
          var urlParams = this.collectUrlParameters();
          if (urlParams) {
            for (var key in urlParams) {
              if (urlParams.hasOwnProperty(key)) {
                updates[key] = urlParams[key];
              }
            }
          }
  
          if (!tracking.visit_time_first) {
            updates.visit_time_first = new Date().toISOString();
            updates.landing_page_first = window.location.pathname;
            
            // Store first touch parameters
            if (urlParams) {
              for (var key in urlParams) {
                if (urlParams.hasOwnProperty(key)) {
                  // Convert _last to _first for first visit
                  var firstKey = key.replace('_last', '_first');
                  updates[firstKey] = urlParams[key];
                }
              }
            }
          }
  
          var mergedData = {};
          for (var key in tracking) {
            if (tracking.hasOwnProperty(key)) {
              mergedData[key] = tracking[key];
            }
          }
          for (var key in updates) {
            if (updates.hasOwnProperty(key)) {
              mergedData[key] = updates[key];
            }
          }
  
          debugLog('Saving merged tracking data', mergedData);
          this.storage.set('gam_user_tracking', mergedData);
        } catch (e) {
          errorLog('Error in trackPageView', e);
        }
      };
  
      GAM.prototype.init = function() {
        try {
          debugLog('Initializing GAM');
          this.trackPageView();
          debugLog('GAM initialization complete');
        } catch (e) {
          errorLog('Error in init', e);
        }
      };
  
      // Initialize GAM
      debugLog('Creating GAM instance');
      window.gam = new GAM(window.GAMConfig);
      debugLog('GAM instance created successfully');
  
    } catch (e) {
      errorLog('Critical error in GAM initialization', e);
    }
  })(window);
  </script>